This is an api which registers the devices which have recently entered the same room as the scanning wireless card. It makes use of 802.11 probe requests to detect this information.


Notes:

Dependencies: 
libpcap
airmon-ng

This script won't put your card in monitor mode. You'll need to use airmon-ng or a similar tool to do so. Also, your scanning card should not be used for anything else. Read: you'll need a second NIC to connect with. 

I advise that someone alter the web frontend if they have a need. This was never intended to be a serious webapp. This can easily become a part of a bigger chain by generating the db.json in another service and using the json api.
